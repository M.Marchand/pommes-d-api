<?php

namespace App\Controller;

use App\Repository\PommeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Pomme;

class APIController extends AbstractController
{
    private $pommeRepository;
    public function __construct(PommeRepository $pommeRepository)
{
    $this->pommeRepository = $pommeRepository;
}




    /**
     * @Route("/API/Pomme", name="a_p_i")
     */
    public function index()
    {
        $arrayPomme = $this->pommeRepository->findAll();
        $this->getDoctrine();

        $arrayPomme = $this->get('serializer')->serialize($arrayPomme, 'json');

        return new JsonResponse(
            $arrayPomme, 200, [], true
        );

    }
}

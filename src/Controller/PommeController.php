<?php

namespace App\Controller;

use App\Repository\PommeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Pomme;


class PommeController extends AbstractController
{
    private $pommeRepository;
    public function __construct(PommeRepository $pommeRepository)
    {
        $this->pommeRepository=$pommeRepository;
    }

    /**
     * @Route("/pomme", name="pomme")
     */
    public function index()
    {
        $pommeArray= $this->pommeRepository->findAll();
        foreach ($pommeArray as $pomme){
            $photo = $pomme->getPhoto();
        }

        return $this->render('pomme/index.html.twig', [
            'controller_name' => 'PommeController',
            'pommeArray'=> $pommeArray,
            'photo'=>$photo
        ]);
    }
}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * @ORM\Entity(repositoryClass="App\Repository\PommeRepository")
 */
/**
 * @ORM\Entity
 * @Vich\Uploadable
 */
class Pomme
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $NomVariete;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Origine;

    /**
     * @ORM\Column(type="string", length=400, nullable=true)
     */
    private $Aspect;

    /**
     * @ORM\Column(type="string", length=400, nullable=true)
     */
    private $Texture;

    /**
     * @ORM\Column(type="string", length=400, nullable=true)
     */
    private $Saveur;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $SaisonMois;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Consommation;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $Photo;
    /**
     * @Vich\UploadableField(mapping="Photo", fileNameProperty="Photo")
     * @var File
     */
    private $photoFile;
    /**
     * @var \DateTime
     */
    private $updatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomVariete(): ?string
    {
        return $this->NomVariete;
    }

    public function setNomVariete(?string $NomVariete): self
    {
        $this->NomVariete = $NomVariete;

        return $this;
    }

    public function getOrigine(): ?string
    {
        return $this->Origine;
    }

    public function setOrigine(?string $Origine): self
    {
        $this->Origine = $Origine;

        return $this;
    }

    public function getAspect(): ?string
    {
        return $this->Aspect;
    }

    public function setAspect(?string $Aspect): self
    {
        $this->Aspect = $Aspect;

        return $this;
    }

    public function getTexture(): ?string
    {
        return $this->Texture;
    }

    public function setTexture(?string $Texture): self
    {
        $this->Texture = $Texture;

        return $this;
    }

    public function getSaveur(): ?string
    {
        return $this->Saveur;
    }

    public function setSaveur(?string $Saveur): self
    {
        $this->Saveur = $Saveur;

        return $this;
    }

    public function getSaisonMois(): ?string
    {
        return $this->SaisonMois;
    }

    public function setSaisonMois(?string $SaisonMois): self
    {
        $this->SaisonMois = $SaisonMois;

        return $this;
    }

    public function getConsommation(): ?string
    {
        return $this->Consommation;
    }

    public function setConsommation(?string $Consommation): self
    {
        $this->Consommation = $Consommation;

        return $this;
    }
    public function getPhoto(): ?string
    {
        return $this->Photo;
    }

    public function setPhoto(?string $Photo): self
    {
        $this->Photo = $Photo;
        if ($Photo) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
        return $this;
    }
    public function getphotoFile()
    {
        return $this->photoFile;
    }

    public function setphotoFile(File $Photo = null)
    {
        $this->photoFile = $Photo;


    }
}
